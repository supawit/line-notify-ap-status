FROM node:10.15.3-slim
COPY app/ /app
WORKDIR /app
RUN npm install
CMD ["node","index"]