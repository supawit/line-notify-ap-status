const snmp = require("./snmp")
const message = require("./message")
const line = require("./line")

// define list of wifi controller from environment variables
const wifiController = JSON.parse(process.env.CONTROLLERS)

// define list of line chat api key from environment variables
const lineApiKey = JSON.parse(process.env.LINEAPIKEYS)

let app = async () => {  
  //get current apName data from snmp
  let apName = []
  for(let controller of wifiController){
    apName = apName.concat(await snmp.getApName(controller.ip, controller.communityString))
  }

  // prepare message for line notify
  let lineMessage = await message.prepare(apName)

  // notify to line chat room
  for(let key of lineApiKey){
    await line.notify(key,lineMessage).then(console.log)
  }
}

app()
setInterval(() => {
  app()
}, 1000 * 60 * process.env.INTERVAL);
