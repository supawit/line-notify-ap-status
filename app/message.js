let message = {}

const arrayCompare = require("array-compare")
const fs = require('fs')

message.prepare = async (apNameData) => {
    return new Promise((resolve, reject) => {
        //create file if not exists
        if(!fs.existsSync('data/apName.json')){
            fs.writeFileSync('data/apName.json', JSON.stringify([]))
        }
        //read latest apName from file  
        fs.readFile('data/apName.json',(errer,data) => {
            if(errer) throw errer

            let latestApName = JSON.parse(data)
            fs.writeFileSync('data/apName.json', JSON.stringify(apNameData))

            //compare latest and current apName
            let result = arrayCompare(latestApName, apNameData, 'apName')
            //console.log({down:JSON.stringify(result.missing), up:JSON.stringify(result.added)})

            //prepare line message
            let downAp = []
            for(let element of result.missing){
            downAp.push(element.a.apName)
            }
            let downMsg = ""
            if(downAp.length){
            downMsg = '\ndown AP: ' + downAp.toString()
            }
            
            let upAp = []
            for(let element of result.added){
            upAp.push(element.b.apName)
            }
            let upMsg = ""
            if(upAp.length){
            upMsg = '\nup AP: ' + upAp.toString()
            }

            let message = {
                message :  downMsg+upMsg
            }
            console.log(downAp, upAp, message)
            resolve(message)
        })
    })
}

module.exports = message