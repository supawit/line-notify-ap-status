let line = {}

const request = require('request')
const queryString = require('querystring')

line.notify = async (lineKey,message) => {
    return new Promise((resolve, reject) => {
        if(message.message.length){
            let postMessage = queryString.stringify(message)
            request(
                {
                  headers: {
                    "Content-Length": postMessage.length,
                    "Content-Type": "application/x-www-form-urlencoded",
                    "Authorization": "Bearer " + lineKey.key
                  },
                  uri: 'https://notify-api.line.me/api/notify',
                  body: postMessage,
                  method: 'POST',
                },
                (errer, reponse, body) => {
                  if(errer) throw errer
                    resolve({
                        chatName:  lineKey.chatName,
                        body: body,
                        status: reponse.headers,
                      }
                    )       
                }
              )

        }else{
            resolve({ chatName: lineKey.chatName, mgs: "message can not be empty.Yes no AP up or down"})
        }
    })
}

module.exports = line