let snmp = {}

const netSnmp = require ('net-snmp')
const bsnAPName = "1.3.6.1.4.1.14179.2.2.1.1.3"

snmp.getApName = async (wifiControllerIp, communityString) => {
    return new Promise((resolve, reject) => {
        //console.log(wifiControllerIp,communityString);
        
        let apName = []
        let session = netSnmp.createSession (wifiControllerIp, communityString, {version: netSnmp.Version2c})
        session.subtree(
            bsnAPName,
            (varbinds)=>{
                for (let i = 0; i < varbinds.length; i++) {
                    if (netSnmp.isVarbindError (varbinds[i])){
                      console.error (netSnmp.varbindError (varbinds[i]))
                    }
                    else{
                        apName.push({"apName":varbinds[i].value.toString('utf8'), controller:wifiControllerIp})
                    }
                }
            },
            (error)=>{
                if (error) {
                    reject(error.toString ())
                } else {

                    resolve(apName)
                }
            }
        )        
    })
}

module.exports = snmp