# Access point up/down status Line notification for Cisco Wireless Controller
*tested on Cisco 8540 Wireless Controller*

## Requirement
* docker
* docker-compose

## Generate LINE Notify Token
* https://notify-bot.line.me/my/

## Installation
```bash
$ git clone https://gitlab.com/supawit/line-notify-ap-status.git
```

## Initialization
rename ***config.env.dist*** to ***config.env*** and edit the value of the variable to match your system.

## Usage
```bash
$ cd /path/to/line-notify-ap-status
$ docker-compose up -d
```